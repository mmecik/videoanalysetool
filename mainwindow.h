#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGroupBox>
#include <QBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QThread>
#include <QSessionManager>
#include <QPlainTextEdit>
#include <string>
#include <iostream>
#include <QSlider>

#include "opencvbridge.h"

class MainWindow : public QWidget
{
    Q_OBJECT

public:
    ~MainWindow();
    MainWindow();

protected:
    void closeEvent(QCloseEvent *event) Q_DECL_OVERRIDE;

private slots:

    /*************************************************************************
     * Actions
     ************************************************************************/

    // Buttons
    void playPauseButtonAction(void);
    void stopButtonAction(void);
    void stepBackwardAction(void);
    void stepForwardAction(void);
    void stepToFrame(void);

    // Menubar
    void menubarOpenFileAction(void);
    void SliderPressedAction(void);
    void SliderReleasedAction(void);

private:

    // UI Objects
    QWidget widget;
    QMenuBar * menubar;
    QLabel * videoFrame;
    QSlider * currentVideoPositionScrollbar;
    // UI Layouts
    QHBoxLayout * buttonBox;

    OpenCvBridge * opencvBridge;

    // Functions
    void createMenubarInterface(void);
    void createButtonInterface(void);
    void createVideoInterface(void);
    void createScrollbarInterface(void);

    QThread * videoThread;

};

#endif // MAINWINDOW_H
