#include "opencvhelpers.h"

QImage OpenCvHelpers::Mat2QImage(Mat &mat){
    // resize
    QImage qimage = QImage(mat.cols,mat.rows, QImage::Format_ARGB32);

    // copying mat to qimage
    for (int i = 0; i < mat.rows; ++i) {
        for (int j = 0; j < mat.cols; ++j) {
            cv::Vec3b bgr = mat.at<cv::Vec3b>(i, j);
            qimage.setPixel(j, i, qRgb(bgr[2], bgr[1], bgr[0]));
        }
    }

    return qimage;
}
