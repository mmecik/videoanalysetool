#ifndef OPENCVHELPERS_H
#define OPENCVHELPERS_H

#include <QImage>
#include <QPixmap>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace cv;

class OpenCvHelpers
{
public:
    QImage Mat2QImage(Mat &mat);
};

#endif // OPENCVHELPERS_H
