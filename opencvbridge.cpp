#include "opencvbridge.h"

/*****************************************************************/
/* Constructors                                                  */
/*****************************************************************/
OpenCvBridge::OpenCvBridge(QObject *parent) :
    QObject(parent)
{
    opencvHelpers = new OpenCvHelpers();
}

/*****************************************************************/
/* Setters                                                       */
/*****************************************************************/

void OpenCvBridge::SetVideoPath(string path){
    _videofilePath = path;
}

void OpenCvBridge::SetVideoStatus(E_VIDEO_STATUS status){
    _videoStatus = status;
}

void OpenCvBridge::SetCurrentVideoVideoFrame(int currentFrameNumber){
    _currentVideoFrameNumber = currentFrameNumber;
}

void OpenCvBridge::SetVideoFrame(int videoFrame, E_VIDEO_DIRECTION direction){

     mutex.lock();
    if (direction == STEP_BACKWARDS){
        _currentVideoFrameNumber = videoFrame+direction+direction;
        _videoSequenceEnd = videoFrame+direction;
    }else{
        _currentVideoFrameNumber = videoFrame;
        _videoSequenceEnd = videoFrame+direction;
    }

    videoCapture.set(CV_CAP_PROP_POS_FRAMES, _currentVideoFrameNumber);
    mutex.unlock();
    cout << "Current-Pos: " <<_currentVideoFrameNumber << endl;
    cout << "NumberOfFrames: " << _videoSequenceEnd << endl;

}


/*****************************************************************/
/* Getters                                                       */
/*****************************************************************/
bool OpenCvBridge::VideoIsPlaying(void){
    return _working;
}

int OpenCvBridge::GetNumberOfVideoFrames(void){

    return _numberOfVideoFrames;
}

int OpenCvBridge::GetCurrentVideoPosition(void){
    return _currentVideoFrameNumber;
}

/*****************************************************************/
/* Helpers                                                       */
/*****************************************************************/
bool OpenCvBridge::VideoInputIsAvailable(void){

    // default value stands for video isnt available
    bool result = false;

    mutex.lock();
    // check if video is already opened, if not, try to open it
    if(!videoCapture.isOpened()){

        // open video
        videoCapture.open(_videofilePath);

        // check if video is available again
        if (videoCapture.isOpened()){

            // get the number of all video frames
            _numberOfVideoFrames = videoCapture.get(CV_CAP_PROP_FRAME_COUNT);

            // if video was paused, start the playback at the last position
            if ( _videoStatus == VIDEO_PAUSE ){
                videoCapture.set(CV_CAP_PROP_POS_FRAMES, _currentVideoFrameNumber);

            // start the video from position 0
            } else if ( _videoStatus == VIDEO_STOP ){
                _currentVideoFrameNumber = 0;
                videoCapture.set(CV_CAP_PROP_POS_FRAMES, _currentVideoFrameNumber);
            }

            _videoStatus = VIDEO_PLAY;

            result= true;
        }
    } else {
        result = true;
    }

    mutex.unlock();

    return result;
}

/****************************************************************/
/* Requests                                                     */
/****************************************************************/
void OpenCvBridge::RequestPlayingVideo(){

    mutex.lock();
    _working = true;
    _abort = false;

    qDebug()<<"Playing video"<<thread()->currentThreadId();
    mutex.unlock();

    // Checking the video input
    if ( VideoInputIsAvailable() ){
        // saving up the frame-count of the video
        _videoSequenceEnd = _numberOfVideoFrames;
        emit SignalPlayVideo();
    }
}

/****************************************************************/
/* Requests                                                     */
/****************************************************************/
void OpenCvBridge::ReqeustPlayingVideoSequence(){

    mutex.lock();
    _working = true;
    _abort = false;

    qDebug()<<"Playing video"<<thread()->currentThreadId();
    mutex.unlock();

    // Checking the video input
    if ( VideoInputIsAvailable() ){
        emit SignalPlayVideo();
    }
}

void OpenCvBridge::RequestPauseVideo(){
    mutex.lock();
    if (_working) {
        _abort = true;
        qDebug()<<"Request to abort the video playing"<<thread()->currentThreadId();
        emit SignalCloseVideo();
    }
    mutex.unlock();
}

void OpenCvBridge::RequestStopVideo(){
    mutex.lock();
    if (_working) {
        _stop = true;
        qDebug()<<"Request to abort the video playing"<<thread()->currentThreadId();
        emit SignalCloseVideo();
    }
    mutex.unlock();
}

/****************************************************************/
/* Actions                                                      */
/****************************************************************/


void OpenCvBridge::SlotPlayingVideo(void){

   qDebug()<<"Starting playing video thread "<<thread()->currentThreadId();

    while ( _currentVideoFrameNumber < _videoSequenceEnd ){


        Mat currentFrame;


        videoCapture >> currentFrame;

        _currentVideoFrameNumber++;

        QImage qimage = opencvHelpers->Mat2QImage(currentFrame);
        QPixmap qpixmap = QPixmap::fromImage(qimage);

        emit SignalVideoChanged(qpixmap);
        emit SignalVideoChanged(_currentVideoFrameNumber);

        bool abort = _abort;
        bool stop = _stop;


        if (abort) {
            qDebug()<<"aborting playing video thread "<<thread()->currentThreadId();
            //videoCapture.release();
            break;
        } else if(stop){
            qDebug()<<"aborting playing video thread "<<thread()->currentThreadId();
            videoCapture.release();
            break;
        }
    }

    //_numberOfVideoFrames = videoCapture.get(CV_CAP_PROP_FRAME_COUNT);

    mutex.lock();
    _working = false;
    _stop = false;
    _abort = false;
    mutex.unlock();


    qDebug()<<"playing video thread finished"<<thread()->currentThreadId();

    emit SignalCloseVideo();


}

