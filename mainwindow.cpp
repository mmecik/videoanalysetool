#include <QtWidgets>
#include <QThread>
#include "mainwindow.h"

using namespace std;

MainWindow::MainWindow()
    : widget(new QWidget)
{
    // init objects
    opencvBridge = new OpenCvBridge();
    videoThread = new QThread();
    opencvBridge->moveToThread(videoThread);

    // set main layout
    QVBoxLayout *mainLayout = new QVBoxLayout();

    // Create menubar and add to layout
    createMenubarInterface();
    mainLayout->addWidget(menubar);

    // creating buttons and add to layout
    createButtonInterface();
    mainLayout->addItem(buttonBox);

    // creating video frame
    createVideoInterface();
    mainLayout->addWidget(videoFrame);

    createScrollbarInterface();
    mainLayout->addWidget(currentVideoPositionScrollbar);

    // Setting up the ui
    setLayout(mainLayout);

    //repaint video frame (label)
    connect(opencvBridge, SIGNAL(SignalVideoChanged(QPixmap)),videoFrame,SLOT(setPixmap(QPixmap)));

    //start video thread
    connect(opencvBridge, SIGNAL(SignalPlayVideo()),videoThread, SLOT(start()));

    //playing video
    connect(videoThread, SIGNAL(started()),opencvBridge, SLOT(SlotPlayingVideo()));

    //stop playing video
    connect(opencvBridge, SIGNAL(SignalCloseVideo()), videoThread, SLOT(quit()), Qt::DirectConnection);

    // creating scrollbar connetions
    connect(currentVideoPositionScrollbar, SIGNAL(sliderPressed()), this,SLOT(SliderPressedAction()));
    connect(currentVideoPositionScrollbar, SIGNAL(sliderReleased()), this,SLOT(SliderReleasedAction()));
    connect(opencvBridge, SIGNAL(SignalVideoChanged(int)), currentVideoPositionScrollbar,SLOT(setValue(int)));

    opencvBridge->SetVideoPath("/home/bender/Downloads/download/FD_vs_WW_HZ2.mp4");
}

MainWindow::~MainWindow()
{

}

/*************************************************************************
 *
 *
 * User-Interface Functions
 *
 *
 ************************************************************************/

void MainWindow::createMenubarInterface(){

    menubar = new QMenuBar();

    QMenu * start = new QMenu("File...");
    QAction * fileChooser = new QAction("Open...",start);

    start->addAction(fileChooser);

    menubar->addMenu(start);

    connect(fileChooser, SIGNAL(triggered()), this, SLOT(menubarOpenFileAction()));
}

void MainWindow::createButtonInterface(){

    buttonBox = new QHBoxLayout();

    // setting up buttons
    QPushButton * paramTrackingButton = new QPushButton(" * ");
    QPushButton * oneStepBackwardButton = new QPushButton(" -1 << ");
    QPushButton * playPauseButton = new QPushButton(" I> / II ");
    QPushButton * oneStepForwardButton = new QPushButton(" >> 1+ ");
    QPushButton * stopButton = new QPushButton(" [ ] ");

    buttonBox->addWidget(paramTrackingButton);
    buttonBox->addWidget(oneStepBackwardButton);
    buttonBox->addWidget(playPauseButton);
    buttonBox->addWidget(oneStepForwardButton);
    buttonBox->addWidget(stopButton);

    // connect actions with buttons
    connect(playPauseButton, SIGNAL(clicked()),this, SLOT(playPauseButtonAction()));
    connect(stopButton, SIGNAL(clicked(bool)),this, SLOT(stopButtonAction()));
    connect(oneStepBackwardButton, SIGNAL(clicked(bool)), this, SLOT(stepBackwardAction()));
    connect(oneStepForwardButton, SIGNAL(clicked(bool)), this,SLOT(stepForwardAction()));
}

void MainWindow::createVideoInterface(void){
    videoFrame = new QLabel("No video input");
    //videoFrame->setFixedSize(QSize(640,480));
    videoFrame->setAlignment(Qt::AlignCenter);
}

void MainWindow::createScrollbarInterface(){
    currentVideoPositionScrollbar = new QSlider(Qt::Horizontal);
    currentVideoPositionScrollbar->setValue(0);
}

 void  MainWindow::SliderReleasedAction(void){
     cout << "Value: " << currentVideoPositionScrollbar->value() << endl;
     opencvBridge->SetVideoFrame(currentVideoPositionScrollbar->value(),STEP_FORWARDS);
     opencvBridge->ReqeustPlayingVideoSequence();
 }

 void MainWindow::SliderPressedAction(void){
     opencvBridge->RequestPauseVideo();
     opencvBridge->SetVideoStatus(VIDEO_PAUSE);
 }

/*************************************************************************
 * Events
 ************************************************************************/
void MainWindow::closeEvent(QCloseEvent *event)
{
    event->accept();
}

/*************************************************************************
 * Actions
 ************************************************************************/


void MainWindow::menubarOpenFileAction(){
    QString qFilename = QFileDialog::getOpenFileName(this,tr("Open Image"), "/home/", tr("VideoFiles (*.MOV)"));
    string path = qFilename.toStdString();
    opencvBridge->SetVideoPath(path);

    cout << "Pfad:" << path << endl;

    videoFrame->setText(qFilename);
}

void MainWindow::playPauseButtonAction(void){

    if ( opencvBridge->VideoIsPlaying() ){
        opencvBridge->RequestPauseVideo();
        opencvBridge->SetVideoStatus(VIDEO_PAUSE);
    } else {
        videoThread->wait(); // If the thread is not running, this will immediately return
        opencvBridge->RequestPlayingVideo();
        opencvBridge->SetVideoStatus(VIDEO_PLAY);
        cout << "Number of Video Frames: " << opencvBridge->GetNumberOfVideoFrames() << endl;
        currentVideoPositionScrollbar->setMaximum(opencvBridge->GetNumberOfVideoFrames());
    }

}

void MainWindow::stepBackwardAction(void){

    opencvBridge->RequestPauseVideo();
    videoThread->wait();
    opencvBridge->SetVideoFrame(opencvBridge->GetCurrentVideoPosition(),STEP_BACKWARDS);
    opencvBridge->ReqeustPlayingVideoSequence();
}

void MainWindow::stepForwardAction(void){

    opencvBridge->RequestPauseVideo();
    videoThread->wait();
    opencvBridge->SetVideoFrame(opencvBridge->GetCurrentVideoPosition(),STEP_FORWARDS);
    opencvBridge->ReqeustPlayingVideoSequence();
}

void MainWindow::stopButtonAction(void){

        currentVideoPositionScrollbar->setValue(0);
        opencvBridge->SetCurrentVideoVideoFrame(0);
        opencvBridge->SetVideoStatus(VIDEO_STOP);
        opencvBridge->RequestPlayingVideo();
        opencvBridge->RequestStopVideo();
}

void MainWindow::stepToFrame(void){

}
