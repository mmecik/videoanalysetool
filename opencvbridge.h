#ifndef OPENCVBRIDGE_H
#define OPENCVBRIDGE_H

#include <QObject>
#include <QMutex>
#include <string>
#include <QTimer>
#include <QEventLoop>
#include <QThread>
#include <QDebug>
#include <QImage>
#include <QPixmap>
#include <QMainWindow>
#include <QSlider>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <string>
#include <iostream>
#include "opencvhelpers.h"

using namespace std;
using namespace cv;

typedef enum {

    VIDEO_PLAY,
    VIDEO_PAUSE,
    VIDEO_STOP,

} E_VIDEO_STATUS;

typedef enum{

    STEP_BACKWARDS = -1,
    STEP_NONE = 0,
    STEP_FORWARDS = 1

} E_VIDEO_DIRECTION;

class OpenCvBridge : public QObject
{
    Q_OBJECT

public:

    /*****************************************************************/
    /* Constructors                                                  */
    /*****************************************************************/
    explicit OpenCvBridge(QObject *parent = 0);

    /****************************************************************/
    /* Setters                                                      */
    /****************************************************************/
    void SetVideoPath(string path);
    void SetVideoStatus(E_VIDEO_STATUS status);
    void SetCurrentVideoVideoFrame(int currentFrameNumber);
    void SetVideoFrame(int videoFrame, E_VIDEO_DIRECTION direction);
    /****************************************************************/
    /* Getters                                                      */
    /****************************************************************/
    bool VideoIsPlaying(void);
    int GetNumberOfVideoFrames(void);
    int GetCurrentVideoPosition(void);

    /*****************************************************************/
    /* Helpers                                                       */
    /*****************************************************************/
    bool VideoInputIsAvailable(void);

    /****************************************************************/
    /* Requests                                                     */
    /****************************************************************/
    void RequestPlayingVideo(void);
    void ReqeustPlayingVideoSequence(void);
    void RequestPauseVideo(void);
    void RequestStopVideo(void);

private slots:

    /****************************************************************/
    /* Actions                                                      */
    /****************************************************************/
    void SlotPlayingVideo(void);

signals:

    void SignalPlayVideo(void);
    void SignalVideoChanged(QPixmap qpixmap);
    void SignalVideoChanged(int number);
    void SignalCloseVideo(void);

private:

    //video player control states
    bool _abort;
    bool _stop;
    bool _working;

    int _currentVideoFrameNumber;
    int _videoSequenceEnd;
    int _numberOfVideoFrames;

    string _videofilePath;

    E_VIDEO_STATUS _videoStatus;

    VideoCapture videoCapture;

    QSize * videoFrameSize;
    QMutex mutex;

    OpenCvHelpers * opencvHelpers;

    QSlider * _currentVideoPositionSlider;
};

#endif // OPENCVBRIDGE_H
